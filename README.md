[![Build status](https://build.appcenter.ms/v0.1/apps/96799c68-4268-4cc5-9554-0a3b4f4ef3ff/branches/master/badge)](https://appcenter.ms)

# WP-tipcalculator
"Calculateur de pourboire" sur Windows Phone

AUTHOR : melvinmajor
PROJECT TITLE : TipCalculator.
PURPOSE OF PROJECT : only for fun.
VERSION : 2015.xxxx.xxxx.xxx
LANGUAGE : FRENCH (for now)

USER INSTRUCTIONS:
------------------
 1.) Download and install Visual Studio Community.
 2.) Download all the files.
 3.) Place all the files to your documents, in Visual Studio folder, Projects
     and create "TipCalculator" folder.
 4.) Click on TipCalculator.sln
 5.) You're ready !

If you just want to test the app, it's available on the Windows Phone Store !

URL:
https://www.microsoft.com/fr-be/store/apps/calculateur-de-pourboire/9nblggh0dth4
Current version : 2014.1223.1331.135
Not currently available on all stores and only in french (for the moment).

COMPATIBILITY:
--------------
This application has been officialy made with Windows Phone 8.1 API.
Based on the API used, this app works pretty well on Windows Phone 8.1 and Windows 10 Mobile.
So, this app is Windows Phone 8.1 Ready but also Windows 10 Mobile Compatible.

DEVICES USED FOR INTERNAL TESTING:
----------------------------------
Here are the following devices used :
 - Windows Phone Emulator (480x800, 512MB RAM)
 - Windows Phone Emulator (1920x1080, 1GB RAM)
 - Samsung ATIV S GT-I8750 (1280x768, 1GB RAM)
 - Microsoft Lumia 950 (2560x1440, 3GB RAM)
 - Microsoft Lumia 950 XL (2560x1440, 3GB RAM)

I have used different ways to fully test this app.
I first used the Windows Phone Emulator with screens of 480x800 resolution and only 512MB of RAM, which are the minimum requirements for Windows Phone 8.1. I then used it with screens of 1920x1080 resolution and 1GB of RAM to see how the app is performing on bigger screens.
After those tests with the emulator system, I  officially published the first final version of the app on the Store but it was buggy only on some Nokia devices. I made further tests with the Samsung ATIV S (GT-I8750) to correct everything and published small improvements.
Afterwards, Windows 10 Mobile was deployed, I made some registry tweaks on the Samsung ATIV S to give it the "eligibility" to receive the update under Dev certifications and Interrop Unlock mode.
I also tested the app on a Microsoft Lumia 950 and also the Lumia 950 XL and everything is working pretty well on Windows 10 Mobile.
